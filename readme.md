# partners

## api design
I prefer to use only POST http methods.

The idea to go out of rest api, it complicates a lot of stuff.
I describe web api like json rpc, it contains the following rules:
- only POST methods
- only 3 status codes:
  - 200 as an expected response
  - 400 as a client error
  - 500 as an internal backend error, basically it means unexpected behaviour
- 2 kind of responses:
  - a common one, just an expected json object
  - http error, a json object describes a client error

## how to test
- make apitest

## how to run locally
- make run_local

### what I would like to improve
- authentication/authorization, now it's supposed to have api gateway on front of the service that just provides user's data.
- apitest shouldn't be in a function like that, it's hard to support that way, also it doesn't clean the application's state.
- I didn't get why we pass "square meters" and "phone number" fields to find a partner, since then I removed them from the request instance (In my opinion we need them to apply for a partner's service)
- I pass user id to the request, I suppose we have an api gateway in front of the service and it exchanges a session token to user id and other fields