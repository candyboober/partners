package main

import (
	"context"
	"errors"
	"github.com/dennypenta/partners/assembly"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func mainContext() context.Context {
	ctx, cancel := context.WithCancel(context.Background())
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-quit
		cancel()
	}()

	return ctx
}

func main() {
	config, err := assembly.NewConfig()
	if err != nil {
		log.Fatalln("failed to create config:", err)
	}
	ctx := mainContext()

	m, err := migrate.New(
		"file://db/migrations",
		config.DbDsn)
	if err != nil {
		log.Fatalln("failed to create migrate instance:", err)
	}
	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		log.Fatalln("failed to u migrations:", err)
	}

	app, err := assembly.New(ctx, config)
	if err != nil {
		log.Fatalln("failed to build an app:", err)
	}

	engine := gin.New()
	engine.GET("/healthz", func(c *gin.Context) {
		c.Status(http.StatusOK)
	})
	engine.POST("/partners/list", app.PartnersHandler.PartnersList)
	engine.POST("/partners/by_id", app.PartnersHandler.PartnersById)

	srv := &http.Server{
		Addr:    config.HttpAddr,
		Handler: engine,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	<-ctx.Done()
	if err := srv.Shutdown(context.Background()); err != nil {
		log.Println("http server forced to shutdown:", err)
	}
	log.Println("the application is being closed")
}
