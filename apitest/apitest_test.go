package apitest_test

import (
	"github.com/dennypenta/partners/apitest/api"
	"github.com/dennypenta/partners/handlers/dto"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"testing"
)

type ApitestSuite struct {
	suite.Suite

	client *api.Client
}

func (s *ApitestSuite) SetupTest() {
	s.client = api.NewClient("http://localhost:8000")
}

func (s *ApitestSuite) TearDownTest() {
	// I have to clear the database here
}

func (s *ApitestSuite) TestTheWholeFlow() {
	// unknown material
	res, _, err := s.client.PartnersList(dto.PartnersListRequest{
		Material: "unknown",
		Location: dto.Point{1, 1},
	})
	s.Require().NoError(err)
	s.Require().Empty(res.Partners)

	// too far
	res, _, err = s.client.PartnersList(dto.PartnersListRequest{
		Material: "wood",
		Location: dto.Point{55, 555},
	})
	s.Require().NoError(err)
	s.Require().Empty(res.Partners)

	res, _, err = s.client.PartnersList(dto.PartnersListRequest{
		Material: "wood",
		Location: dto.Point{1, 1},
	})
	s.Require().NoError(err)
	s.Require().Len(res.Partners, 3)
	s.Equal(res.Partners[0].Rating, 3)
	s.Equal(res.Partners[1].Rating, 2)
	s.Equal(res.Partners[2].Rating, 1)

	_, httpErr, err := s.client.PartnerById(uuid.New())
	s.Require().Equal(httpErr.Code, dto.ErrorCodePartnerNotFound)

	partnerDetails, _, err := s.client.PartnerById(res.Partners[0].Id)
	s.Require().NoError(err)
	s.Require().Equal(partnerDetails.Partner, res.Partners[0])
}

func TestApi(t *testing.T) {
	suite.Run(t, new(ApitestSuite))
}
