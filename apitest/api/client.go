package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dennypenta/partners/handlers/dto"
	"github.com/google/uuid"
	"net/http"
)

type Client struct {
	c *http.Client

	url string
}

func NewClient(url string) *Client {
	return &Client{
		c:   http.DefaultClient,
		url: url,
	}
}

func (c *Client) PartnersList(req dto.PartnersListRequest) (partner dto.PartnersListResponse, httpErr dto.HttpError, err error) {
	body, err := json.Marshal(req)

	res, err := c.c.Post(c.url+"/partners/list", "application/json", bytes.NewReader(body))
	if err != nil {
		return partner, httpErr, fmt.Errorf("failed to a send request to get partners list: %w", err)
	}

	if res.StatusCode != http.StatusOK {
		if err := json.NewDecoder(res.Body).Decode(&httpErr); err != nil {
			return partner, httpErr, fmt.Errorf("failed to decode http error: %w", err)
		}

		return partner, httpErr, err
	}

	if err := json.NewDecoder(res.Body).Decode(&partner); err != nil {
		return partner, httpErr, fmt.Errorf("failed to decode partners list: %w", err)
	}

	return partner, httpErr, nil
}

func (c *Client) PartnerById(id uuid.UUID) (partner dto.PartnerByIdResponse, httpErr dto.HttpError, err error) {
	body, err := json.Marshal(dto.PartnerByIdRequest{
		PartnerId: id,
	})
	if err != nil {
		return partner, httpErr, err
	}
	res, err := c.c.Post(c.url+"/partners/by_id", "application/json", bytes.NewReader(body))
	if err != nil {
		return partner, httpErr, fmt.Errorf("failed to a send request to get partner by id: %w", err)
	}

	if res.StatusCode != http.StatusOK {
		if err := json.NewDecoder(res.Body).Decode(&httpErr); err != nil {
			return partner, httpErr, fmt.Errorf("failed to decode http error: %w", err)
		}

		return partner, httpErr, err
	}

	if err := json.NewDecoder(res.Body).Decode(&partner); err != nil {
		return partner, httpErr, fmt.Errorf("failed to decode partner by id: %w", err)
	}

	return partner, httpErr, nil
}
