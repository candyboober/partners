package assembly

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	DbDsn    string `envconfig:"DB_DSN" required:"true"`
	HttpAddr string `envconfig:"HTTP_ADDR" default:":8000"`
}

func NewConfig() (Config, error) {
	var config Config
	if err := envconfig.Process("", &config); err != nil {
		return config, fmt.Errorf("failed to process config: %w", err)
	}

	return config, nil
}
