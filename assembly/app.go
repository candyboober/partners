package assembly

import (
	"context"
	"fmt"
	"github.com/dennypenta/partners/db"
	"github.com/dennypenta/partners/handlers"
	"github.com/dennypenta/partners/services"
	"github.com/jackc/pgx/v4"
	"github.com/rs/zerolog"
	"os"
)

type App struct {
	PartnersHandler *handlers.PartnersHandler
}

func New(ctx context.Context, config Config) (*App, error) {
	l := zerolog.New(os.Stderr)
	pg, err := pgx.Connect(ctx, config.DbDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to connect pgx: %w", err)
	}
	postgres := db.NewPostgres(pg)
	partnerService := services.NewPartnerService(postgres)
	partnerHandler := handlers.NewPartnersHandler(partnerService, l.With().Str("component", "handler").Logger())

	return &App{
		PartnersHandler: partnerHandler,
	}, nil
}
