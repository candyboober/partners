build:
	@ GOOS=linux GOARCH=amd64 go build -o bin/api cmd/main.go

apitest: build
	@ docker-compose --profile=apitest up --build -d
	@ echo "Waiting for service to be healthy"
	@ until $$(curl --output /dev/null --silent --fail http://localhost:8000/healthz); do printf '.'; sleep 1; done && echo "Service Ready!"
	@ go test --race -v ./apitest
	@ docker-compose down

run_local:
	@ docker-compose up -d
	@ env $$(cat .env.local | grep -v ^\\# | xargs) go run cmd/main.go
