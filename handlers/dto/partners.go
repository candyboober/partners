package dto

import "github.com/google/uuid"

type PartnersListRequest struct {
	// In a real app I would do the following,
	// Material would be in the request
	Material string `json:"material"`
	// Location we can get from some a customer profile table/service based on a user id or session token or whatever else.
	Location Point `json:"location"`
}

type PartnersListResponse struct {
	Partners []Partner `json:"partners"`
}

type PartnerByIdRequest struct {
	PartnerId uuid.UUID `json:"partnerId"`
}

type PartnerByIdResponse struct {
	Partner Partner `json:"partner"`
}

type Partner struct {
	Id              uuid.UUID `json:"id"`
	Location        Point     `json:"point"`
	Materials       []string  `json:"materials"`
	OperatingRadius int       `json:"operatingRadius"`
	Rating          int       `json:"rating"`
}

type Point [2]float64
