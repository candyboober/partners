package dto

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

const (
	ErrorCodeInvalidPartnersListFilter = "INVALID_PARTNERS_LIST_FILTER"
	ErrorCodeInvalidPartnerId          = "INVALID_PARTNER_ID"
	ErrorCodePartnerNotFound           = "INVALID_PAY_IN"
)

type HttpError struct {
	Code string
}

func SendError(c *gin.Context, code string) {
	c.JSON(http.StatusBadRequest, &HttpError{
		Code: code,
	})
}
