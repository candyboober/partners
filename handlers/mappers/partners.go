package mappers

import (
	"github.com/dennypenta/partners/handlers/dto"
	"github.com/dennypenta/partners/models"
)

func PartnersListFilterFromDto(req dto.PartnersListRequest) models.PartnersListFilter {
	return models.PartnersListFilter{
		Material: req.Material,
		Location: models.Point(req.Location),
	}
}

func PartnersToDto(pp []models.Partner) []dto.Partner {
	res := make([]dto.Partner, len(pp))

	for i := range res {
		res[i] = PartnerToDto(pp[i])
	}

	return res
}

func PartnerToDto(p models.Partner) dto.Partner {
	return dto.Partner{
		Id:              p.Id,
		Materials:       p.Materials,
		Location:        dto.Point(p.Location),
		OperatingRadius: p.OperatingRadius,
		Rating:          p.Rating,
	}
}
