package handlers

import (
	"context"
	"errors"
	"github.com/dennypenta/partners/handlers/dto"
	"github.com/dennypenta/partners/handlers/mappers"
	"github.com/dennypenta/partners/models"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"net/http"
)

type PartnerService interface {
	PartnersList(ctx context.Context, filter models.PartnersListFilter) ([]models.Partner, error)
	PartnerById(ctx context.Context, id uuid.UUID) (models.Partner, error)
}

type PartnersHandler struct {
	partnerService PartnerService

	l zerolog.Logger
}

func NewPartnersHandler(partnerService PartnerService, l zerolog.Logger) *PartnersHandler {
	return &PartnersHandler{partnerService: partnerService, l: l}
}

func (h *PartnersHandler) PartnersList(c *gin.Context) {
	var req dto.PartnersListRequest
	if err := c.BindJSON(&req); err != nil {
		dto.SendError(c, dto.ErrorCodeInvalidPartnersListFilter)
		return
	}

	filter := mappers.PartnersListFilterFromDto(req)

	partners, err := h.partnerService.PartnersList(c.Request.Context(), filter)
	if err != nil {
		h.l.Err(err).
			Str("handler", "PartnersList").
			Msg("failed to get partners list")
		c.Status(http.StatusInternalServerError)
		return
	}

	dtoPartners := mappers.PartnersToDto(partners)

	c.JSON(http.StatusOK, dto.PartnersListResponse{
		Partners: dtoPartners,
	})
}

func (h *PartnersHandler) PartnersById(c *gin.Context) {
	var req dto.PartnerByIdRequest
	if err := c.BindJSON(&req); err != nil {
		dto.SendError(c, dto.ErrorCodeInvalidPartnerId)
		return
	}

	partner, err := h.partnerService.PartnerById(c.Request.Context(), req.PartnerId)
	if err != nil {
		if errors.Is(err, models.ErrPartnerNotFound) {
			dto.SendError(c, dto.ErrorCodePartnerNotFound)
			return
		}
		h.l.Err(err).
			Str("handler", "PartnersById").
			Stringer("partner_id", req.PartnerId).
			Msg("failed to get a partner by id")
		c.Status(http.StatusInternalServerError)
		return
	}

	dtoPartner := mappers.PartnerToDto(partner)

	c.JSON(http.StatusOK, dto.PartnerByIdResponse{
		Partner: dtoPartner,
	})
}
