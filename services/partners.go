package services

import (
	"context"
	"fmt"
	"github.com/dennypenta/partners/models"
	"github.com/google/uuid"
)

type PartnersRepository interface {
	PartnersList(ctx context.Context, filter models.PartnersListFilter) ([]models.Partner, error)
	PartnerById(ctx context.Context, id uuid.UUID) (models.Partner, error)
}

type PartnerService struct {
	repo PartnersRepository
}

func NewPartnerService(repo PartnersRepository) *PartnerService {
	return &PartnerService{
		repo: repo,
	}
}

func (s *PartnerService) PartnersList(ctx context.Context, filter models.PartnersListFilter) ([]models.Partner, error) {
	partner, err := s.repo.PartnersList(ctx, filter)
	if err != nil {
		return partner, fmt.Errorf("failed to get partners list: %w", err)
	}

	return partner, nil
}

func (s *PartnerService) PartnerById(ctx context.Context, id uuid.UUID) (models.Partner, error) {
	partner, err := s.repo.PartnerById(ctx, id)
	if err != nil {
		return partner, fmt.Errorf("failed to get partner: %w", err)
	}

	return partner, nil
}
