FROM alpine:3.14

WORKDIR /

COPY bin/api /usr/bin/api
RUN chmod +x /usr/bin/api

COPY db/migrations db/migrations

CMD ["/usr/bin/api"]
