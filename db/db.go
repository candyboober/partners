package db

import (
	"context"
	"errors"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/dennypenta/partners/models"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type Postgres struct {
	pg *pgx.Conn
	sq sq.StatementBuilderType
}

func NewPostgres(pg *pgx.Conn) *Postgres {
	return &Postgres{
		pg: pg,
		sq: sq.StatementBuilder.PlaceholderFormat(sq.Dollar),
	}
}

func (p *Postgres) PartnersList(ctx context.Context, filter models.PartnersListFilter) ([]models.Partner, error) {
	// I decided to use ST_Distance_Sphere instead of  ST_Distance_Spheroid
	// since it works a bit faster and we will not call the service through the ocean
	query, args, err := p.sq.Select("id", "materials", "location", "radius", "rating").
		From("partners").
		Where("? = any(materials) and radius >= ST_DistanceSphere(geometry(location), geometry(point(?, ?)))",
			filter.Material,
			filter.Location[0], filter.Location[1]).
		OrderBy("rating desc").
		ToSql()
	if err != nil {
		return nil, fmt.Errorf("failed to build partners list query: %w", err)
	}

	rows, err := p.pg.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to query partners list: %w", err)
	}
	defer func() {
		rows.Close()
	}()

	var res []models.Partner
	for rows.Next() {
		var partner models.Partner
		if err := rows.Scan(&partner.Id, &partner.Materials, &partner.Location, &partner.OperatingRadius, &partner.Rating); err != nil {
			return nil, fmt.Errorf("failed to scan partners list: %w", err)
		}

		res = append(res, partner)
	}

	return res, nil
}

func (p *Postgres) PartnerById(ctx context.Context, id uuid.UUID) (models.Partner, error) {
	query, args, err := p.sq.Select("id", "materials", "location", "radius", "rating").
		From("partners").
		Where(sq.Eq{"id": id}).ToSql()
	if err != nil {
		return models.Partner{}, fmt.Errorf("failed to build get partner query: %w", err)
	}

	var partner models.Partner
	if err := p.pg.QueryRow(ctx, query, args...).
		Scan(&partner.Id, &partner.Materials, &partner.Location, &partner.OperatingRadius, &partner.Rating); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return partner, models.ErrPartnerNotFound
		}
		return partner, fmt.Errorf("failed to scan get partner: %w", err)
	}

	return partner, nil
}
