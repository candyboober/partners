CREATE EXTENSION IF NOT EXISTS postgis;

CREATE TABLE partners (
    id uuid PRIMARY KEY default gen_random_uuid(),
    materials varchar(40)[],
    location point NOT NULL,
    radius integer NOT NULL,
    rating smallint default 0
);

-- insert test data
-- In a real application I would do it in a api tests initialization
-- but this trick allow me not to create one more postgres connection
-- so it's only for the test challenge.

insert into partners(materials, location, radius, rating) values
(ARRAY['wood', 'carpet'], point(1, 1), 10000, 3),
(ARRAY['wood', 'carpet'], point(1, 1), 10000, 2),
(ARRAY['wood', 'carpet'], point(1, 1), 10000, 1),
(ARRAY['carpet'], point(1, 1), 10000, 3),
(ARRAY['wood'], point(2, 2), 10000, 3);