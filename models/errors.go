package models

import "errors"

var (
	ErrPartnerNotFound = errors.New("partner not found")
)
