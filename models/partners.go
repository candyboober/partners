package models

import (
	"fmt"
	"github.com/google/uuid"
	"strconv"
	"strings"
)

// Point describes geo point objects as (lon, lat)
type Point [2]float64

func (p *Point) Scan(src interface{}) error {
	v, ok := src.(string)
	if !ok {
		return fmt.Errorf("Point.Scan: string expected")
	}

	v = strings.TrimLeft(v, "(")
	v = strings.TrimRight(v, ")")
	values := strings.Split(v, ",")
	if len(values) != 2 {
		return fmt.Errorf("Point.Scan: 2 values in a point expected")
	}

	p1, err := strconv.ParseFloat(values[0], 64)
	if err != nil {
		return fmt.Errorf("Point.Scan: failed to parse float: %w", err)
	}
	p2, err := strconv.ParseFloat(values[0], 64)
	if err != nil {
		return fmt.Errorf("Point.Scan: failed to parse float: %w", err)
	}
	p[0] = p1
	p[1] = p2

	return nil
}

//
//func (p *Point) Scan(src interface{}) error {
//	println()
//	pq.Float64Array
//	return nil
//}

type Partner struct {
	Id              uuid.UUID
	Materials       []string
	Location        Point
	OperatingRadius int
	Rating          int
}

type PartnersListFilter struct {
	Material string
	Location Point
}
